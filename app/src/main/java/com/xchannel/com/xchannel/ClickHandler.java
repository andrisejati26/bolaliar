package com.xchannel.com.xchannel;

public interface ClickHandler {
    void onItemClicked(int position);
}