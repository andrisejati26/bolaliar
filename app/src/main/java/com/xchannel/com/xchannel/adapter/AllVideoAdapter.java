package com.xchannel.com.xchannel.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.xchannel.com.xchannel.ClickHandler;
import com.xchannel.com.xchannel.Constant;
import com.xchannel.com.xchannel.R;
import com.xchannel.com.xchannel.model.Video;

import java.util.List;

public class AllVideoAdapter extends RecyclerView.Adapter<AllVideoAdapter.ItemViewHolder> {
    private final List<Video> dataList;
    private final Context context;
    private final ClickHandler mClickHandler;

    public AllVideoAdapter(Context ctx, List<Video> data, ClickHandler clickHandler) {
        context = ctx;
        dataList = data;
        LayoutInflater mInflater = LayoutInflater.from(context);
        mClickHandler = clickHandler;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_video, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        Glide.with(context)
                .load(dataList.get(position).getThumbVideo())
                .thumbnail(0.01f)
                .into(holder.image);

        holder.judul.setText(dataList.get(position).getVideoTitle());
    }

    @Override
    public int getItemCount() {
        //return dataList.size();
        return (dataList== null) ? 0 : dataList.size();
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final ImageView image;
        private final TextView judul;

        ItemViewHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);

            image = itemView.findViewById(R.id.thumbnailVideo);
            judul = itemView.findViewById(R.id.judulVideo);
        }

        @Override
        public void onClick(View v) {
            if (mClickHandler != null) mClickHandler.onItemClicked(getAdapterPosition());
        }
    }

}
