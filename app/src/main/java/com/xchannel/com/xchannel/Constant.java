package com.xchannel.com.xchannel;

public class Constant {
    public static final String BASE_URL_API = "http://202.78.200.228/bolaliarapi/";
    public static final String BASE_URL = "http://bolaliar.com";

    public static final String BASE_URL_IMAGE_RADIO = BASE_URL + "/po-content/uploads/";
    public static final String BASE_URL_IMAGE_VIDEO = BASE_URL_API + "upload/list_video/";
    public static final String BASE_URL_IMAGE_THUMBNAIL = BASE_URL_API + "upload/";
    public static final String BASE_URL_IMAGE_BANNER = BASE_URL_API + "upload/banner/";
    public static final String BASE_URL_IMAGE_PODCAST = BASE_URL_API + "upload/list_podcast/";
    public static final String BASE_URL_IMAGE_HOST = BASE_URL_API + "upload/list_host/";

    public static final String BASE_URL_API_NEWS = "https://newsapi.org/v2/";
    public static final String API_KEY_NEWS = "de3280449ec449c58a804e9f33c08db5";

    public static final String SLIDER_IMAGE="image_banner";
    public static final String SLIDER_LINK="url_banner";

    public static final String SLIDER_URL = BASE_URL_API +"api/api.php?banner";

}
