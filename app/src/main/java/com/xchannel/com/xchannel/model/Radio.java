package com.xchannel.com.xchannel.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Radio {
    @SerializedName("id")
    @Expose
    private String idheadnews;

    @SerializedName("title")
    @Expose
    private String titleheadnews;

    @SerializedName("isiPost")
    @Expose
    private String isipost;

    @SerializedName("imgUrl")
    @Expose
    private String ImageHeadNews;


    public String getIdheadnews() {
        return idheadnews;
    }

    public String getTitleHeadNews() {
        return titleheadnews;
    }

    public String getIsipost() {
        return isipost;
    }

    public String getImageHeadNews() {
        return ImageHeadNews;
    }

}
