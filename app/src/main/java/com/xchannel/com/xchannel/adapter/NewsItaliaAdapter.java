package com.xchannel.com.xchannel.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.xchannel.com.xchannel.ClickHandler;
import com.xchannel.com.xchannel.R;
import com.xchannel.com.xchannel.model.NewsItalia;

import java.util.List;

public class NewsItaliaAdapter extends RecyclerView.Adapter<NewsItaliaAdapter.ItemViewHolder>{

    private final List<NewsItalia> dataList;
    private final Context context;
    private final ClickHandler mClickHandler;

    public NewsItaliaAdapter(Context ctx, List<NewsItalia> data, ClickHandler clickHandler) {
        context = ctx;
        dataList = data;
        LayoutInflater mInflater = LayoutInflater.from(context);
        mClickHandler = clickHandler;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final ImageView image;
        private final TextView livestream;

        ItemViewHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);

            image = itemView.findViewById(R.id.listview_image);
            livestream = itemView.findViewById(R.id.listview_item_title);
        }

        @Override
        public void onClick(View v) {
            if (mClickHandler != null) mClickHandler.onItemClicked(getAdapterPosition());
        }
    }

    @Override
    public NewsItaliaAdapter.ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_siaran, parent, false);
        return new NewsItaliaAdapter.ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(NewsItaliaAdapter.ItemViewHolder holder, int position) {
        Glide.with(context)
                .load(dataList.get(position).getimage())
                .thumbnail(0.01f)
                .into(holder.image);

        holder.livestream.setText(dataList.get(position).gettitle());
    }

    @Override
    public int getItemCount() {
        //return dataList.size();
        return (dataList== null) ? 0 : dataList.size();
    }
}
