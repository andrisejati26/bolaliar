package com.xchannel.com.xchannel.ui.activity;

import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;

import android.widget.LinearLayout;

import com.xchannel.com.xchannel.R;

public class AllNewsActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    private RecyclerView recyclerViewArticle;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private LinearLayout mEmptyView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_news);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_arrow_back_white_24dp);
        upArrow.setColorFilter(getResources().getColor(android.R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        toolbar.setTitle("XChannel News");

        recyclerViewArticle = findViewById(R.id.recyclerViewNews);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerViewArticle.setLayoutManager(linearLayoutManager);

        mSwipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_red_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_green_light,
                android.R.color.holo_blue_light);

        mSwipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshLayout.setRefreshing(true);
                loadArticle();
            }
        });

        mSwipeRefreshLayout.setRefreshing(false);
        mEmptyView = findViewById(R.id.emptyView);


    }

    private void loadArticle() {
       /* GetDataService service = RetrofitClientInstance.getRetrofitInstance("https://newsapi.org/v2/").create(GetDataService.class);

        Call<News> call = service.getAllNews("id", "entertainment", "de3280449ec449c58a804e9f33c08db5");
        call.enqueue(new Callback<News>() {
            @Override
            public void onResponse(Call<News> call, Response<News> response) {
                Log.d("RadioFragment", "The response of news api : " + String.valueOf(response.body()));
                if (response.body().getStatus().equals("ok")) {
                    mSwipeRefreshLayout.setRefreshing(false);
                    List<Article> articleList = response.body().getArticles();
                    generateNewsList(articleList);
                }
            }

            @Override
            public void onFailure(Call<News> call, Throwable throwable) {
                mSwipeRefreshLayout.setRefreshing(false);
                FancyToast.makeText(AllNewsActivity.this, "Something went wrong...Please try later!", FancyToast.LENGTH_SHORT).show();
                Log.d("RadioFragment", "The error is : " + throwable.getMessage());
            }
        });*/
    }
/*

    private void generateNewsList(final List<Article> articleList) {
       */
/* if (articleList.isEmpty()) {
            recyclerViewArticle.setVisibility(View.GONE);
            mEmptyView.setVisibility(View.VISIBLE);
        } else {
            AllNewsAdapter newsAdapter = new AllNewsAdapter(this, articleList, new ClickHandler() {
                @Override
                public void onItemClicked(int position) {
                    String url = articleList.get(position).getUrl();
                    Intent intent = new Intent(AllNewsActivity.this, ViewNewsActivity.class);
                    intent.putExtra("newsUrl", url);
                    startActivity(intent);
                    overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
                }
            });

            recyclerViewArticle.setAdapter(newsAdapter);
        }*//*

    }
*/

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onRefresh() {
        loadArticle();
    }
}
