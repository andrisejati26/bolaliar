package com.xchannel.com.xchannel.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.xchannel.com.xchannel.ClickHandler;
import com.xchannel.com.xchannel.Constant;
import com.xchannel.com.xchannel.R;
import com.xchannel.com.xchannel.model.Radio;

import java.util.List;

public class FragmentRadioAdapter extends RecyclerView.Adapter<FragmentRadioAdapter.ItemViewHolder> {
    private final List<Radio> dataList;
    private final Context context;
    private final ClickHandler mClickHandler;

    public FragmentRadioAdapter(Context ctx, List<Radio> data, ClickHandler clickHandler) {
        context = ctx;
        dataList = data;
        mClickHandler = clickHandler;
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private final ImageView iconRadio;
        private final TextView namaRadio;

        ItemViewHolder(View itemView) {
            super(itemView);

            iconRadio = itemView.findViewById(R.id.imageMain);
            namaRadio = itemView.findViewById(R.id.nama_radio);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mClickHandler != null) mClickHandler.onItemClicked(getAdapterPosition());
        }
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.grid_radio, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ItemViewHolder holder, int position) {
        holder.namaRadio.setText(dataList.get(position).getIsipost());

        Glide.with(context)
                .load(Constant.BASE_URL_IMAGE_RADIO + dataList.get(position).getImageHeadNews())
                .thumbnail(0.01f)
                .into(holder.iconRadio);
    }

    @Override
    public int getItemCount() {
        return (dataList== null) ? 0 : dataList.size();
    }
}
