package com.xchannel.com.xchannel.networking;

import retrofit2.Retrofit;

public class RetrofitHolder {
    Retrofit retrofit;

    public Retrofit getRetrofit() {
        return retrofit;
    }

    public void setRetrofit(Retrofit retrofit) {
        this.retrofit = retrofit;
    }
}
