package com.xchannel.com.xchannel.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NewsItalia {



    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("isiPost")
    @Expose
    private String isipost;
    @SerializedName("imgURL")
    @Expose
    private String image;
    @SerializedName("thumbs")
    @Expose
    private String thumb;
    @SerializedName("categoryName")
    @Expose
    private String categoryname;


    public String id() {
        return id;
    }

    public void setid(String id) {
        this.id = id;
    }

    public String gettitle() {
        return title;
    }

    public void settitle(String title) {
        this.title = title;
    }

    public String getIsipost() {
        return isipost;
    }

    public void setIsipost(String isipost) {
        this.isipost = isipost;
    }

    public String getimage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }


    public String getThumb() {
        return thumb;
    }

    public void setThumb(String urlToImage) {
        this.thumb = urlToImage;
    }


    public String getCategoryname() {
        return categoryname;
    }

    public void setCategoryname(String categoryname) {
        this.categoryname = categoryname;
    }



}
