package com.xchannel.com.xchannel.ui.fragment;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.shashank.sony.fancytoastlib.FancyToast;
import com.xchannel.com.xchannel.ClickHandler;
import com.xchannel.com.xchannel.Constant;
import com.xchannel.com.xchannel.ItemSlider;
import com.xchannel.com.xchannel.JsonUtils;
import com.xchannel.com.xchannel.R;
import com.xchannel.com.xchannel.adapter.HeadNewsAdapter;
import com.xchannel.com.xchannel.adapter.NewsEropaAdapter;
import com.xchannel.com.xchannel.adapter.NewsIndonesiaAdapter;
import com.xchannel.com.xchannel.adapter.NewsInggrisAdapter;
import com.xchannel.com.xchannel.adapter.NewsItaliaAdapter;
import com.xchannel.com.xchannel.adapter.NewsJermanAdapter;
import com.xchannel.com.xchannel.adapter.NewsSpanyolAdapter;
import com.xchannel.com.xchannel.model.HeadNews;
import com.xchannel.com.xchannel.model.NewsEropa;
import com.xchannel.com.xchannel.model.NewsIndonesia;
import com.xchannel.com.xchannel.model.NewsInggris;
import com.xchannel.com.xchannel.model.NewsItalia;
import com.xchannel.com.xchannel.model.NewsJerman;
import com.xchannel.com.xchannel.model.NewsSpanyol;
import com.xchannel.com.xchannel.networking.GetDataService;
import com.xchannel.com.xchannel.networking.RetrofitClientInstance;
import com.xchannel.com.xchannel.ui.activity.AllNewsActivity;
import com.xchannel.com.xchannel.ui.activity.ViewNewsActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressLint("ValidFragment")
public class RadioFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener ,  BaseSliderView.OnSliderClickListener {
    // RecycleView
    private RecyclerView rView;
    private RecyclerView rView1;
    private RecyclerView rView2;
    private RecyclerView rView3;
    private RecyclerView rView4;
    private RecyclerView rView5;
    private RecyclerView rVieweropa;





    private TextView noInternet;
    private TextView emptyViewPodcast;
    private TextView emptyViewNews;

    private TextView newsshowmore;


    private SliderLayout mDemoSlider;
    List<ItemSlider> arrayofSlider;
    ItemSlider itemSlider;
    RelativeLayout mainlay;
    ProgressBar pbar;


    private SwipeRefreshLayout mSwipeRefreshLayout;


    public RadioFragment() {
    }

    public RadioFragment(int position) {
        int wizard_page_position = position;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        int layout_id = R.layout.fragment_radio;
        View view = inflater.inflate(layout_id, container, false);

        emptyViewNews = view.findViewById(R.id.emptyViewNews);
        emptyViewPodcast = view.findViewById(R.id.emptyViewPodcast);
        newsshowmore = view.findViewById(R.id.showmore_news);



        mDemoSlider = (SliderLayout) view.findViewById(R.id.slider);
        pbar = (ProgressBar) view.findViewById(R.id.progressBar);
        mainlay = (RelativeLayout) view.findViewById(R.id.Banner);
        arrayofSlider = new ArrayList<>();


        rView = view.findViewById(R.id.recyclerViewradio);
        LinearLayoutManager layoutManager2 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        rView.setLayoutManager(layoutManager2);




        rVieweropa = view.findViewById(R.id.recyclerVieweropa);
        LinearLayoutManager layoutManagereropa= new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        rVieweropa.setLayoutManager(layoutManagereropa);



        rView1 = view.findViewById(R.id.recyclerViewNews);

        GridLayoutManager lLayout1 = new GridLayoutManager(getActivity(), 1);
    //    LinearLayoutManager lLayout1 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        rView1.setLayoutManager(lLayout1);



        rView2 = view.findViewById(R.id.recyclerViewInggris);


        LinearLayoutManager lLayout2 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        rView2.setLayoutManager(lLayout2);
//        rView2.setNestedScrollingEnabled(false);


        rView3 = view.findViewById(R.id.recyclerViewItalia);

        GridLayoutManager lLayout3 = new GridLayoutManager(getActivity(), 1);
        //LinearLayoutManager lLayout3 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        rView3.setLayoutManager(lLayout3);

        rView4 = view.findViewById(R.id.recyclerViewSpanyol);
        LinearLayoutManager lLayout4 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        rView4.setLayoutManager(lLayout4);

        rView5 = view.findViewById(R.id.recyclerViewJerman);
        LinearLayoutManager lLayout5 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        rView5.setLayoutManager(lLayout5);

        noInternet = view.findViewById(R.id.textView2);

        mSwipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_red_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_green_light,
                android.R.color.holo_blue_light);

        mSwipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshLayout.setRefreshing(true);

                loadPodcast();
                loadHeadNews();
                loadNewsInggris();
                loadNewsIndonesia();
                loadNewsItalia();
                loadNewsSpanyol();
                loadNewsJerman();
                loadNewsEropa();
            }


        });


        newsshowmore.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(view.getContext(), AllNewsActivity.class);
                startActivityForResult(myIntent, 0);
            }
        });



        if (JsonUtils.isNetworkAvailable(getActivity())) {
            new MyTaskSlider().execute(Constant.SLIDER_URL);
        } else {

        }

        return view;
    }



    private void loadPodcast() {

    }

    private void loadHeadNews() {
        mSwipeRefreshLayout.setRefreshing(true);

        GetDataService service = RetrofitClientInstance.getRetrofitInstance(Constant.BASE_URL_API).create(GetDataService.class);

        Call<List<HeadNews>> call = service.getAllHeadNews();
        call.enqueue(new Callback<List<HeadNews>>() {
            @Override
            public void onResponse(Call<List<HeadNews>> call, Response<List<HeadNews>> response) {
                rView.setVisibility(View.VISIBLE);
                mSwipeRefreshLayout.setRefreshing(false);

                generateHeadNewsList(response.body());
                Log.d("RadioFragment", "Get response, the response is : " + response.body());
                noInternet.setVisibility(View.GONE);


            }

            @Override
            public void onFailure(Call<List<HeadNews>> call, Throwable t) {
                mSwipeRefreshLayout.setRefreshing(false);
                FancyToast.makeText(getActivity(), "Something went wrong...Please try later!", FancyToast.LENGTH_SHORT).show();
                Log.d("RadioFragment", "Cannot access the server, the problem is : " + t);
                rView.setVisibility(View.GONE);
                noInternet.setVisibility(View.VISIBLE);

            }
        });
    }




    private void generateHeadNewsList(final List<HeadNews> dataList) {
        HeadNewsAdapter headnewsAdapter = new HeadNewsAdapter(getActivity(), dataList, new ClickHandler() {
            @Override
            public void onItemClicked(int position) {

                Intent intent = new Intent(getActivity(), ViewNewsActivity.class);
                intent.putExtra("judul", dataList.get(position).gettitle());
                intent.putExtra("gambar", dataList.get(position).getimage());
                intent.putExtra("isiPost", dataList.get(position).getIsipost());
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
            }
        });

        rView.setAdapter(headnewsAdapter);





    }




    private void loadNewsEropa() {
        mSwipeRefreshLayout.setRefreshing(true);

        GetDataService service = RetrofitClientInstance.getRetrofitInstance(Constant.BASE_URL_API).create(GetDataService.class);

        Call<List<NewsEropa>> call = service.getAllNewsEropa();
        call.enqueue(new Callback<List<NewsEropa>>() {
            @Override
            public void onResponse(Call<List<NewsEropa>> call, Response<List<NewsEropa>> response) {
                rView.setVisibility(View.VISIBLE);
                mSwipeRefreshLayout.setRefreshing(false);

                generateNewsEropaList(response.body());
                Log.d("RadioFragment", "Get response, the response is : " + response.body());
                noInternet.setVisibility(View.GONE);


            }

            private void generateNewsEropaList(final List<NewsEropa> datalist) {



                NewsEropaAdapter eropaadapter = new NewsEropaAdapter(getActivity(), datalist, new ClickHandler() {
                    @Override
                    public void onItemClicked(int position) {

                        Intent intent = new Intent(getActivity(), ViewNewsActivity.class);
                        intent.putExtra("judul", datalist.get(position).gettitle());
                        intent.putExtra("gambar", datalist.get(position).getimage());
                        intent.putExtra("isiPost", datalist.get(position).getIsipost());
                        startActivity(intent);
                        getActivity().overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
                    }
                });

                rVieweropa.setAdapter(eropaadapter);




            }


            @Override
            public void onFailure(Call<List<NewsEropa>> call, Throwable t) {
                mSwipeRefreshLayout.setRefreshing(false);
                FancyToast.makeText(getActivity(), "Something went wrong...Please try later!", FancyToast.LENGTH_SHORT).show();
                Log.d("RadioFragment", "Cannot access the server, the problem is : " + t);
                rVieweropa.setVisibility(View.GONE);
                noInternet.setVisibility(View.VISIBLE);

            }
        });
    }



    private void loadNewsIndonesia() {
        mSwipeRefreshLayout.setRefreshing(true);

        GetDataService service = RetrofitClientInstance.getRetrofitInstance(Constant.BASE_URL_API).create(GetDataService.class);

        Call<List<NewsIndonesia>> call = service.getAllNewsIndonesia();
        call.enqueue(new Callback<List<NewsIndonesia>>() {
            @Override
            public void onResponse(Call<List<NewsIndonesia>> call, Response<List<NewsIndonesia>> response) {
                rView2.setVisibility(View.VISIBLE);
                mSwipeRefreshLayout.setRefreshing(false);

                generateDataIndonesiaList(response.body());
                Log.d("RadioFragment", "Get response, the response is : " + response.body());
                noInternet.setVisibility(View.GONE);


            }

            @Override
            public void onFailure(Call<List<NewsIndonesia>> call, Throwable t) {
                mSwipeRefreshLayout.setRefreshing(false);
                FancyToast.makeText(getActivity(), "Something went wrong...Please try later!", FancyToast.LENGTH_SHORT).show();
                Log.d("RadioFragment", "Cannot access the server, the problem is : " + t);
                rView2.setVisibility(View.GONE);
                noInternet.setVisibility(View.VISIBLE);

            }
        });
    }




    private void generateDataIndonesiaList(final List<NewsIndonesia> dataList) {
        NewsIndonesiaAdapter mAdapter = new NewsIndonesiaAdapter(getActivity(), dataList, new ClickHandler() {
            @Override
            public void onItemClicked(int position) {

                Intent intent = new Intent(getActivity(), ViewNewsActivity.class);
                intent.putExtra("judul", dataList.get(position).gettitle());
                intent.putExtra("gambar", dataList.get(position).getimage());
                intent.putExtra("isiPost", dataList.get(position).getIsipost());
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
            }
        });

        rView1.setAdapter(mAdapter);


    }



    private void loadNewsInggris() {
        mSwipeRefreshLayout.setRefreshing(true);

      GetDataService service = RetrofitClientInstance.getRetrofitInstance(Constant.BASE_URL_API).create(GetDataService.class);

        Call<List<NewsInggris>> call = service.getAllNewsInggris();
        call.enqueue(new Callback<List<NewsInggris>>() {
            @Override
            public void onResponse(Call<List<NewsInggris>> call, Response<List<NewsInggris>> response) {
                rView2.setVisibility(View.VISIBLE);
                mSwipeRefreshLayout.setRefreshing(false);
                generateNewsInggrisList(response.body());
                Log.d("RadioFragment", "Get response, the response is : " + response.body());
                noInternet.setVisibility(View.GONE);


            }

            @Override
            public void onFailure(Call<List<NewsInggris>> call, Throwable t) {
                mSwipeRefreshLayout.setRefreshing(false);
                FancyToast.makeText(getActivity(), "Something went wrong...Please try later!", FancyToast.LENGTH_SHORT).show();
                Log.d("RadioFragment", "Cannot access the server, the problem is : " + t);
                rView2.setVisibility(View.GONE);
                noInternet.setVisibility(View.VISIBLE);

            }
        });
    }


    private void generateNewsInggrisList(final List<NewsInggris> articleInggrisList) {
        if (articleInggrisList.isEmpty()) {
            rView2.setVisibility(View.GONE);
            emptyViewNews.setVisibility(View.VISIBLE);
            emptyViewNews.setText("News is cooming soon");
        } else {


            NewsInggrisAdapter newsinggrisadapter = new NewsInggrisAdapter(getActivity(), articleInggrisList, new ClickHandler() {
                @Override
                public void onItemClicked(int position) {

                    Intent intent = new Intent(getActivity(), ViewNewsActivity.class);
                    intent.putExtra("judul", articleInggrisList.get(position).gettitle());
                    intent.putExtra("gambar", articleInggrisList.get(position).getimage());
                    intent.putExtra("isiPost", articleInggrisList.get(position).getIsipost());
                    startActivity(intent);
                    getActivity().overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
                }
            });

            rView2.setAdapter(newsinggrisadapter);


        }
    }




    private void loadNewsItalia() {
        mSwipeRefreshLayout.setRefreshing(true);

        GetDataService service = RetrofitClientInstance.getRetrofitInstance(Constant.BASE_URL_API).create(GetDataService.class);

        Call<List<NewsItalia>> call = service.getAllNewsItalia();
        call.enqueue(new Callback<List<NewsItalia>>() {
            @Override
            public void onResponse(Call<List<NewsItalia>> call, Response<List<NewsItalia>> response) {
                rView3.setVisibility(View.VISIBLE);
                mSwipeRefreshLayout.setRefreshing(false);
                generateNewsItaliaList(response.body());
                Log.d("RadioFragment", "Get response, the response is : " + response.body());
                noInternet.setVisibility(View.GONE);


            }

            @Override
            public void onFailure(Call<List<NewsItalia>> call, Throwable t) {
                mSwipeRefreshLayout.setRefreshing(false);
                FancyToast.makeText(getActivity(), "Something went wrong...Please try later!", FancyToast.LENGTH_SHORT).show();
                Log.d("RadioFragment", "Cannot access the server, the problem is : " + t);
                rView3.setVisibility(View.GONE);
                noInternet.setVisibility(View.VISIBLE);

            }
        });
    }


    private void generateNewsItaliaList(final List<NewsItalia> articleItaliaList) {
        if (articleItaliaList.isEmpty()) {
            rView3.setVisibility(View.GONE);
            emptyViewNews.setVisibility(View.VISIBLE);
            emptyViewNews.setText("News is cooming soon");
        } else {


            NewsItaliaAdapter newsItaliaAdapter = new NewsItaliaAdapter(getActivity(), articleItaliaList, new ClickHandler() {
                @Override
                public void onItemClicked(int position) {

                    Intent intent = new Intent(getActivity(), ViewNewsActivity.class);
                    intent.putExtra("judul", articleItaliaList.get(position).gettitle());
                    intent.putExtra("gambar", articleItaliaList.get(position).getimage());
                    intent.putExtra("isiPost", articleItaliaList.get(position).getIsipost());
                    startActivity(intent);
                    getActivity().overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
                }
            });

            rView3.setAdapter(newsItaliaAdapter);


        }
    }





    private void loadNewsSpanyol() {
        mSwipeRefreshLayout.setRefreshing(true);

        GetDataService service = RetrofitClientInstance.getRetrofitInstance(Constant.BASE_URL_API).create(GetDataService.class);

        Call<List<NewsSpanyol>> call = service.getAllNewsSpanyol();
        call.enqueue(new Callback<List<NewsSpanyol>>() {
            @Override
            public void onResponse(Call<List<NewsSpanyol>> call, Response<List<NewsSpanyol>> response) {
                rView4.setVisibility(View.VISIBLE);
                mSwipeRefreshLayout.setRefreshing(false);
                generateNewsSpanyolList(response.body());
                Log.d("RadioFragment", "Get response, the response is : " + response.body());
                noInternet.setVisibility(View.GONE);


            }

            @Override
            public void onFailure(Call<List<NewsSpanyol>> call, Throwable t) {
                mSwipeRefreshLayout.setRefreshing(false);
                FancyToast.makeText(getActivity(), "Something went wrong...Please try later!", FancyToast.LENGTH_SHORT).show();
                Log.d("RadioFragment", "Cannot access the server, the problem is : " + t);
                rView4.setVisibility(View.GONE);
                noInternet.setVisibility(View.VISIBLE);

            }
        });
    }


    private void generateNewsSpanyolList(final List<NewsSpanyol> articleSpanyolList) {
        if (articleSpanyolList.isEmpty()) {
            rView3.setVisibility(View.GONE);
            emptyViewNews.setVisibility(View.VISIBLE);
            emptyViewNews.setText("News is cooming soon");
        } else {


            NewsSpanyolAdapter newsSpanyolAdapter = new NewsSpanyolAdapter(getActivity(), articleSpanyolList, new ClickHandler() {
                @Override
                public void onItemClicked(int position) {


                    Intent intent = new Intent(getActivity(), ViewNewsActivity.class);
                    intent.putExtra("judul", articleSpanyolList.get(position).gettitle());
                    intent.putExtra("gambar", articleSpanyolList.get(position).getimage());
                    intent.putExtra("isiPost", articleSpanyolList.get(position).getIsipost());
                    startActivity(intent);
                    getActivity().overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
                }
            });

            rView4.setAdapter(newsSpanyolAdapter);


        }
    }




    private void loadNewsJerman() {
        mSwipeRefreshLayout.setRefreshing(true);

        GetDataService service = RetrofitClientInstance.getRetrofitInstance(Constant.BASE_URL_API).create(GetDataService.class);

        Call<List<NewsJerman>> call = service.getAllNewsJerman();
        call.enqueue(new Callback<List<NewsJerman>>() {
            @Override
            public void onResponse(Call<List<NewsJerman>> call, Response<List<NewsJerman>> response) {
                rView5.setVisibility(View.VISIBLE);
                mSwipeRefreshLayout.setRefreshing(false);
                generateNewsJermanList(response.body());
                Log.d("RadioFragment", "Get response, the response is : " + response.body());
                noInternet.setVisibility(View.GONE);


            }

            @Override
            public void onFailure(Call<List<NewsJerman>> call, Throwable t) {
                mSwipeRefreshLayout.setRefreshing(false);
                FancyToast.makeText(getActivity(), "Something went wrong...Please try later!", FancyToast.LENGTH_SHORT).show();
                Log.d("RadioFragment", "Cannot access the server, the problem is : " + t);
                rView5.setVisibility(View.GONE);
                noInternet.setVisibility(View.VISIBLE);

            }
        });
    }


    private void generateNewsJermanList(final List<NewsJerman> articleJermanList) {
        if (articleJermanList.isEmpty()) {
            rView5.setVisibility(View.GONE);
            emptyViewNews.setVisibility(View.VISIBLE);
            emptyViewNews.setText("News is cooming soon");
        } else {


            NewsJermanAdapter newsJermanAdapter = new NewsJermanAdapter(getActivity(), articleJermanList, new ClickHandler() {
                @Override
                public void onItemClicked(int position) {


                    Intent intent = new Intent(getActivity(), ViewNewsActivity.class);
                    intent.putExtra("judul", articleJermanList.get(position).gettitle());
                    intent.putExtra("gambar", articleJermanList.get(position).getimage());
                    intent.putExtra("isiPost", articleJermanList.get(position).getIsipost());
                    startActivity(intent);
                    getActivity().overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
                }
            });

            rView5.setAdapter(newsJermanAdapter);


        }
    }







    @Override
    public void onRefresh() {
        loadHeadNews();
        loadPodcast();
        loadNewsInggris();
        loadNewsIndonesia();
        loadNewsItalia();
        loadNewsSpanyol();
        loadNewsJerman();
        loadNewsEropa();
    }

    private class MyTaskSlider extends AsyncTask<String, Void, String> {





        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            pbar.setVisibility(View.VISIBLE);
            mainlay.setVisibility(View.GONE);
        }

        @Override
        protected String doInBackground(String... params) {
            return JsonUtils.getJSONString(params[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            pbar.setVisibility(View.INVISIBLE);
            mainlay.setVisibility(View.VISIBLE);

            if (null == result || result.length() == 0) {



            } else {

                try {
                    JSONArray mainJson = new JSONArray(result);
                    JSONObject objJson = null;
                    for (int i = 0; i < mainJson.length(); i++) {
                        objJson = mainJson.getJSONObject(i);

                        ItemSlider objItem = new ItemSlider();
                        objItem.setImage(objJson.getString(Constant.SLIDER_IMAGE));
                        objItem.setLink(objJson.getString(Constant.SLIDER_LINK));
                        arrayofSlider.add(objItem);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                setAdapterToFeatured();
            }

        }
    }

    public void setAdapterToFeatured() {

        for (int i = 0; i < arrayofSlider.size(); i++) {
            itemSlider = arrayofSlider.get(i);
            DefaultSliderView defaultSliderView = new DefaultSliderView(getActivity());
            defaultSliderView.image(Constant.BASE_URL_IMAGE_BANNER + itemSlider.getImage().toString().replace(" ", "%20"));
            defaultSliderView.setScaleType(BaseSliderView.ScaleType.Fit);
            defaultSliderView.bundle(new Bundle());
            defaultSliderView.getBundle().putString("extra", itemSlider.getLink());
            defaultSliderView.setOnSliderClickListener(this);
            mDemoSlider.addSlider(defaultSliderView);
        }

       mDemoSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        mDemoSlider.setCustomAnimation(new DescriptionAnimation());


    }

    @Override
    public void onSliderClick(BaseSliderView slider) {
        startActivity(new Intent(
                Intent.ACTION_VIEW,
                Uri.parse(slider.getBundle().getString("extra"))));

    }

}
