package com.xchannel.com.xchannel.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Host {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("nama_host")
    @Expose
    private String namaHost;
    @SerializedName("deskripsi_host")
    @Expose
    private String deskripsiHost;
    @SerializedName("image_host")
    @Expose
    private String imageHost;

    public String getId() {
        return id;
    }

    public String getNamaHost() {
        return namaHost;
    }

    public String getDeskripsiHost() {
        return deskripsiHost;
    }

    public String getImageHost() {
        return imageHost;
    }

}