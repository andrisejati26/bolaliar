package com.xchannel.com.xchannel.networking;

import java.util.concurrent.TimeUnit;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClientInstance {
    private static Retrofit retrofit;

//    public static Retrofit getRetrofitInstance(String BASE_URL){
//        if (retrofit == null) {
//            retrofit = new retrofit2.Retrofit.Builder()
//                    .baseUrl(BASE_URL)
//                    .addConverterFactory(GsonConverterFactory.create())
//                    .build();
//        }
//        return retrofit;
//    }

    public static Retrofit getRetrofitInstance(String BASE_URL) {
        RetrofitHolder retrofitHolder = new RetrofitHolder();

        okhttp3.OkHttpClient client = new okhttp3.OkHttpClient.Builder()
                .connectTimeout(5, TimeUnit.MINUTES) // Change it as per your requirement
                .readTimeout(5, TimeUnit.MINUTES)// Change it as per your requirement
                .writeTimeout(5, TimeUnit.MINUTES)// Change it as per your requirement
                .build();

        if (retrofitHolder.retrofit == null) {
            retrofitHolder.retrofit = new retrofit2.Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(client)
                    .build();
        }
        return retrofitHolder.retrofit;
    }

}
