package com.xchannel.com.xchannel.ui.fragment;

import android.annotation.SuppressLint;

import android.content.Intent;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.preference.PreferenceManager;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;
import com.shashank.sony.fancytoastlib.FancyToast;
import com.xchannel.com.xchannel.ClickHandler;
import com.xchannel.com.xchannel.Constant;
import com.xchannel.com.xchannel.R;
import com.xchannel.com.xchannel.adapter.VideoAdapter;

import com.xchannel.com.xchannel.model.Host;
import com.xchannel.com.xchannel.model.Video;
import com.xchannel.com.xchannel.networking.GetDataService;
import com.xchannel.com.xchannel.networking.RetrofitClientInstance;

import com.xchannel.com.xchannel.ui.activity.VideoPlayActivity;
import com.xchannel.com.xchannel.ui.activity.ViewNewsActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

@SuppressLint("ValidFragment")
public class LivestreamFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener {



    private RecyclerView HostView;
    private RecyclerView videoRecyclerView;
    private RecyclerView siaranRecyclerView;

    private SwipeRefreshLayout mSwipeRefreshLayout;

    // Thumbnail Video
    private ImageView thumbVideo;

    // Youtube Player
    private YouTubePlayer mYouTubePlayer;
    private FrameLayout youtubeFrameLayout;

    // Video Player
    private VideoView videoView;

    public LivestreamFragment() { }

    public LivestreamFragment(int position) {
        int wizard_page_position = position;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        int layout_id = R.layout.fragment_livestream;
        View view = inflater.inflate(layout_id, container, false);

        videoRecyclerView = view.findViewById(R.id.recyclerViewVideo);
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        videoRecyclerView.setLayoutManager(layoutManager1);

        // Host Object


        LinearLayoutManager layoutManager2 = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);



        thumbVideo = view.findViewById(R.id.thumb_video);
        //thumbVideo.setVisibility(View.GONE);
        videoView = view.findViewById(R.id.view_video_server);
        youtubeFrameLayout = view.findViewById(R.id.view_video_youtube);



        mSwipeRefreshLayout = view.findViewById(R.id.swipeRefreshLayoutVideo);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_red_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_green_light,
                android.R.color.holo_blue_light);

        mSwipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshLayout.setRefreshing(true);

                loadData();

            }
        });

        mSwipeRefreshLayout.setRefreshing(false);

        return view;
    }







    private void loadData() {
        mSwipeRefreshLayout.setRefreshing(true);

        GetDataService service = RetrofitClientInstance.getRetrofitInstance(Constant.BASE_URL_API).create(GetDataService.class);

        Call<List<Video>> call = service.getAllVideo();
        call.enqueue(new Callback<List<Video>>() {
            @Override
            public void onResponse(Call<List<Video>> call, Response<List<Video>> response) {
                mSwipeRefreshLayout.setRefreshing(false);
                generateDataList(response.body());
            }

            @Override
            public void onFailure(Call<List<Video>> call, Throwable t) {
                mSwipeRefreshLayout.setRefreshing(false);
          }
        });
    }




    private void generateDataList(final List<Video> dataList) {
        VideoAdapter videoAdapter = new VideoAdapter(getActivity(), dataList, new ClickHandler() {
            @Override
            public void onItemClicked(int position) {

                String urlVideo = dataList.get(position).getUrlVideo();

                Intent intent = new Intent(getContext(), VideoPlayActivity.class);
                intent.putExtra("url_video", dataList.get(position).getUrlVideo());
                intent.putExtra("video_type", dataList.get(position).getVideoType());
                startActivity(intent);



            }
        });

        videoRecyclerView.setAdapter(videoAdapter);
    }

    private void playServerVideo(String urlVideo) {

        youtubeFrameLayout.setVisibility(View.GONE);
        thumbVideo.setVisibility(View.GONE);

        videoView.setVisibility(View.VISIBLE);
        videoView.setVideoPath(urlVideo);

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                MediaController mediaController = new MediaController(getActivity());
                videoView.setMediaController(mediaController);
                mediaController.setAnchorView(videoView);
            }
        });

        videoView.start();
    }



    @Override
    public void onRefresh() {

        loadData();
    }
    }







