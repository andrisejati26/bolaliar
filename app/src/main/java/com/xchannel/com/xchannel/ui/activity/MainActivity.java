package com.xchannel.com.xchannel.ui.activity;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;

import com.google.android.material.navigation.NavigationView;
import com.google.android.material.tabs.TabLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.facebook.login.LoginManager;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.xchannel.com.xchannel.BuildConfig;
import com.xchannel.com.xchannel.R;
import com.xchannel.com.xchannel.session.SharedPrefManager;
import com.xchannel.com.xchannel.ui.fragment.LivestreamFragment;
import com.xchannel.com.xchannel.ui.fragment.RadioFragment;
import com.xchannel.com.xchannel.ui.fragment.wagsfragment;

import org.jsoup.Jsoup;

import java.io.IOException;
import java.util.HashMap;
import java.util.concurrent.ExecutionException;

public class MainActivity extends AppCompatActivity {

    private FirebaseRemoteConfig mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
    private HashMap<String, Object> firebaseDefaultMap;
    public static final String VERSION_CODE_KEY = "latest_app_version";

    private static final String TAG = "MainActivity";
    private static final String[] pageTitle = {"Prediksi", "Video","WAGS"};
    private DrawerLayout drawerLayout;
    NavigationView navigationView;
    private InterstitialAd mInterstitialAd;

    private SharedPrefManager sharedPrefManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sharedPrefManager = new SharedPrefManager(this);

        prepareads();
//        CheckUPdate();




        Toolbar toolbar = findViewById(R.id.toolbar);
        final DrawerLayout drawerLayout = findViewById(R.id.drawer_layout);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) actionBar.setDisplayShowTitleEnabled(false);

        NavigationView navigationView = findViewById(R.id.navigation_view);
        TabLayout tabLayout = findViewById(R.id.tab_layout_activity3);
        ViewPager viewPager = findViewById(R.id.viewPager);
        viewPager.setAdapter(new ViewPagerAdapter(getSupportFragmentManager()));
        viewPager.addOnPageChangeListener(new WizardPageChangeListener());
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setCurrentItem(0);

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                drawerLayout.closeDrawers();
                switch (menuItem.getItemId()) {
                    case R.id.menu_profile:
                        Intent login = new Intent(MainActivity.this, SignInActivity.class);
                        startActivity(login);
                        return true;
                    case R.id.menu_video:
                        Intent i = new Intent(MainActivity.this, AllVideoActivity.class);
                        startActivity(i);
                        return true;
                    case R.id.menu_news:
                        Intent intent = new Intent(MainActivity.this, AllNewsActivity.class);
                        startActivity(intent);
                        return true;
                    case R.id.menu_logout:
                        signOut();
                        return true;
                    default:
                        return true;
                }
            }
        });

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };

        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();

        // Set the profile in nav drawer
        View headerView = navigationView.getHeaderView(0);

        ImageView profileImage = headerView.findViewById(R.id.image_profile);
        TextView nameTextView = headerView.findViewById(R.id.name_textview);
        TextView emailTextView = headerView.findViewById(R.id.email_textview);

        Glide.with(this)
                .load(sharedPrefManager.getPhotoUrl())
                .thumbnail(0.01f)
                .into(profileImage);

        nameTextView.setText(sharedPrefManager.getName());
        emailTextView.setText((sharedPrefManager.getEmail().equalsIgnoreCase("-")) ? sharedPrefManager.getPhoneNumber() : sharedPrefManager.getEmail());


        Log.i(TAG, "Phone number is: " + sharedPrefManager.getPhoneNumber());


    }

        private void CheckUPdate() {
            VersionChecker versionChecker = new VersionChecker();
            try
            {   String appVersionName = BuildConfig.VERSION_NAME;
                String mLatestVersionName = versionChecker.execute().get();
                if(!appVersionName.equals(mLatestVersionName)){
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(MainActivity.this);
                    alertDialog.setTitle("Please update your app");
                    alertDialog.setMessage("This app version is no longer supported. Please update your app from the Play Store.");
                    alertDialog.setPositiveButton("UPDATE NOW", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            final String appPackageName = getPackageName();
                            try {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                            } catch (android.content.ActivityNotFoundException anfe) {
                                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                            }
                        }
                    });
                    alertDialog.show();
                }

            } catch (InterruptedException | ExecutionException e) {
                e.printStackTrace();
            }
        }

        @SuppressLint("StaticFieldLeak")
        public class VersionChecker extends AsyncTask<String, String, String> {
            private String newVersion;
            @Override
            protected String doInBackground(String... params) {

                try {
                    newVersion = Jsoup.connect("https://play.google.com/store/apps/details?id="+getPackageName())
                            .timeout(30000)
                            .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                            .referrer("http://www.google.com")
                            .get()
                            .select(".hAyfc .htlgb")
                            .get(7)
                            .ownText();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return newVersion;
            }
        }



    private void prepareads() {
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-2771047182591085/2085172280");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());
    }

    private class ViewPagerAdapter extends FragmentPagerAdapter {
        private final int WIZARD_PAGES_COUNT = 3;

        ViewPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new RadioFragment(position);
                case 1:
                   return new LivestreamFragment(position);
                case 2:
                    return new wagsfragment(position);
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return WIZARD_PAGES_COUNT;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return pageTitle[position];
        }
    }

    private class WizardPageChangeListener implements ViewPager.OnPageChangeListener {

        @Override
        public void onPageScrollStateChanged(int position) {
            // TODO Auto-generated method stub
        }

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            // TODO Auto-generated method stub

        }

        @Override
        public void onPageSelected(int position) {

        }
    }

  /*  @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.search_menu, menu);
        return true;
    }*/

/*    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                Toast.makeText(this, "action clicked!", Toast.LENGTH_SHORT).show();
                break;
            default:
                break;
        }
        return true;
    }*/

    @Override
    protected void onStart() {
        super.onStart();

        if (!sharedPrefManager.getIsSignedIn()) startActivity(new Intent(this, SignInActivity.class));

        Log.i("ProfileActivity", sharedPrefManager.getName());
        Log.i("ProfileActivity", String.valueOf(sharedPrefManager.getIsSignedIn()));
        Log.i("ProfileActivity", String.valueOf(sharedPrefManager.getMethod()));
    }

    private void signOut() {
        FirebaseAuth.getInstance().signOut();
        LoginManager.getInstance().logOut();
        sharedPrefManager.saveSharedPrefBoolean(SharedPrefManager.IS_SIGNEDIN_KEY, false);
        startActivity(new Intent(MainActivity.this, SignInActivity.class));
        finish();
    }


}
