package com.xchannel.com.xchannel.ui.activity;

import androidx.fragment.app.FragmentTransaction;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.Toast;
import android.widget.VideoView;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;
import com.xchannel.com.xchannel.Constant;
import com.xchannel.com.xchannel.R;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class VideoPlayActivity extends AppCompatActivity {

    // Thumbnail Video
    private ImageView thumbVideo;

    // Youtube Player
    private YouTubePlayer mYouTubePlayer;
    private FrameLayout youtubeFrameLayout;

    // Video Player
    private VideoView videoView;

    private String mUrlVideo;
    private String mVideoType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_video_play);

        thumbVideo = findViewById(R.id.thumb_video);
        //thumbVideo.setVisibility(View.GONE);
        videoView = findViewById(R.id.view_video_server);

        Intent intent = getIntent();
        mUrlVideo = intent.getStringExtra("url_video");
        mVideoType = intent.getStringExtra("video_type");


            playServerVideo(mUrlVideo);

    }

    private void playServerVideo(String urlVideo) {



        thumbVideo.setVisibility(View.GONE);

        videoView.setVisibility(View.VISIBLE);
        videoView.setVideoPath(urlVideo);
        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                MediaController mediaController = new MediaController(VideoPlayActivity.this);
                videoView.setMediaController(mediaController);
                mediaController.setAnchorView(videoView);
            }
        });

        videoView.start();
    }

}
