package com.xchannel.com.xchannel.ui.activity;

import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;

import com.xchannel.com.xchannel.ClickHandler;
import com.xchannel.com.xchannel.Constant;
import com.xchannel.com.xchannel.R;
import com.xchannel.com.xchannel.adapter.AllVideoAdapter;
import com.xchannel.com.xchannel.model.Video;
import com.xchannel.com.xchannel.networking.GetDataService;
import com.xchannel.com.xchannel.networking.RetrofitClientInstance;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AllVideoActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {
    private AllVideoAdapter allVideoAdapter;
    private RecyclerView recyclerViewVideo;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private LinearLayout mEmptyView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_video);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        final Drawable upArrow = getResources().getDrawable(R.drawable.ic_arrow_back_white_24dp);
        upArrow.setColorFilter(getResources().getColor(android.R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        recyclerViewVideo = findViewById(R.id.recyclerViewVideo);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        recyclerViewVideo.setLayoutManager(linearLayoutManager);

        mSwipeRefreshLayout = findViewById(R.id.swipeRefreshLayout);
        mSwipeRefreshLayout.setOnRefreshListener(this);
        mSwipeRefreshLayout.setColorSchemeResources(android.R.color.holo_red_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_green_light,
                android.R.color.holo_blue_light);

        mSwipeRefreshLayout.post(new Runnable() {
            @Override
            public void run() {
                mSwipeRefreshLayout.setRefreshing(true);

                loadVideo();
            }
        });

        mSwipeRefreshLayout.setRefreshing(false);

        mEmptyView = findViewById(R.id.emptyView);
    }

    private void loadVideo() {
        GetDataService service = RetrofitClientInstance.getRetrofitInstance(Constant.BASE_URL_API).create(GetDataService.class);

        Call<List<Video>> call = service.getAllVideo();
        call.enqueue(new Callback<List<Video>>() {
            @Override
            public void onResponse(Call<List<Video>> call, Response<List<Video>> response) {
                mSwipeRefreshLayout.setRefreshing(false);
                generateDataListSiaran(response.body());
            }

            @Override
            public void onFailure(Call<List<Video>> call, Throwable t) {
                mSwipeRefreshLayout.setRefreshing(false);
                //FancyToast.makeText(AllVideoActivity.this, "Something went wrong...Please try later!", FancyToast.LENGTH_SHORT).show();
                recyclerViewVideo.setVisibility(View.GONE);
                mEmptyView.setVisibility(View.VISIBLE);
            }
        });
    }

    private void generateDataListSiaran(final List<Video> dataList) {

        if (dataList.isEmpty()) {
            recyclerViewVideo.setVisibility(View.GONE);
            mEmptyView.setVisibility(View.VISIBLE);
        } else {
            allVideoAdapter = new AllVideoAdapter(this, dataList, new ClickHandler() {
                @Override
                public void onItemClicked(int position) {
                    Intent intent = new Intent(AllVideoActivity.this, VideoPlayActivity.class);
                    intent.putExtra("url_video", dataList.get(position).getUrlVideo());
                    intent.putExtra("video_type", dataList.get(position).getVideoType());
                    startActivity(intent);
                }
            });
        }

        recyclerViewVideo.setAdapter(allVideoAdapter);

    }

    @Override
    public void onRefresh() {
        loadVideo();
    }
}
