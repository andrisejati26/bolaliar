package com.xchannel.com.xchannel.ui.activity;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import com.google.android.material.snackbar.Snackbar;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.xchannel.com.xchannel.R;
import com.xchannel.com.xchannel.session.SharedPrefManager;

import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;

public class SignInActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener, View.OnClickListener {
    private static final String TAG = "SignInActivity";

    // Firebase Auth
    private FirebaseAuth mAuth;

    private CallbackManager callbackManager;

    // Google Sign In
    private GoogleSignInClient mGoogleSignInClient;
    private final int RC_SIGN_IN = 32;

    private View mProgressBar;

    private View mSignInMethod;
    private SharedPrefManager sharedPrefManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();
        mProgressBar = findViewById(R.id.progressBar);
        mSignInMethod = findViewById(R.id.sign_in_method);

        // Google Sign In
        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        // Configure Google Sign In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso);

        // Set the dimensions of the sign-in button.
        SignInButton mSignInButton = findViewById(R.id.sign_in_button);
        mSignInButton.setOnClickListener(this);

        // Guest Login
        TextView mGuestLogin = findViewById(R.id.guest_login);
        mGuestLogin.setOnClickListener(this);

        // Phone Login
        Button mPhoneLogin = findViewById(R.id.login_phone_button);
        mPhoneLogin.setOnClickListener(this);

        sharedPrefManager = new SharedPrefManager(this);

        // Facebook Sign In Api
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        // Facebook Login
        callbackManager = CallbackManager.Factory.create();
        // Facebook Sign In API
        LoginButton loginButton = findViewById(R.id.login_button);
        loginButton.setReadPermissions("email", "public_profile");
        // Callback registration
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d(TAG, "facebook:onSuccess:" + loginResult);
                handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Log.d(TAG, "facebook:onCancel:" + "Login Cancel");
            }

            @Override
            public void onError(FacebookException error) {
                Log.d(TAG, "facebook:onError", error);
            }

        });

    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();

                            // Put into sharedpreferences
                            sharedPrefManager.saveSharedPrefString(SharedPrefManager.NAME_KEY, user.getDisplayName());
                            sharedPrefManager.saveSharedPrefString(SharedPrefManager.EMAIL_KEY, user.getEmail());
                            sharedPrefManager.saveSharedPrefString(SharedPrefManager.PHOTO_URL_KEY, String.valueOf(user.getPhotoUrl()));
                            sharedPrefManager.saveSharedPrefInt(SharedPrefManager.METHOD_KEY, SharedPrefManager.GOOGLE_METHOD);
                            sharedPrefManager.saveSharedPrefBoolean(SharedPrefManager.IS_SIGNEDIN_KEY, true);

                            updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Snackbar.make(findViewById(R.id.main_layout), "Authentication Failed.", Snackbar.LENGTH_SHORT).show();
                            updateUI(null);
                        }

                        // ...
                    }
                });
    }

    private void handleFacebookAccessToken(AccessToken token) {
        Log.d(TAG, "handleFacebookAccessToken:" + token);

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithCredential:success");
                            FirebaseUser user = mAuth.getCurrentUser();

                            // Put into sharedpreferences

                            sharedPrefManager.saveSharedPrefString(SharedPrefManager.NAME_KEY, user.getDisplayName());
                            sharedPrefManager.saveSharedPrefString(SharedPrefManager.EMAIL_KEY, user.getEmail());
                            sharedPrefManager.saveSharedPrefString(SharedPrefManager.PHOTO_URL_KEY, String.valueOf(user.getPhotoUrl()));
                            sharedPrefManager.saveSharedPrefInt(SharedPrefManager.METHOD_KEY, SharedPrefManager.FACEBOOK_METHOD);
                            sharedPrefManager.saveSharedPrefBoolean(SharedPrefManager.IS_SIGNEDIN_KEY, true);

                            updateUI(user);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithCredential:failure", task.getException());
                            Toast.makeText(SignInActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            updateUI(null);
                        }
                    }

                });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.sign_in_button:
                signIn(SharedPrefManager.GOOGLE_METHOD);
                break;
            case SharedPrefManager.FACEBOOK_METHOD:
                break;
            case R.id.login_phone_button:
                startActivity(new Intent(SignInActivity.this, PhoneAuthActivity.class));
                finish();
                break;
            case R.id.guest_login:
                signIn(SharedPrefManager.GUEST_METHOD);
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        callbackManager.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = task.getResult(ApiException.class);
                firebaseAuthWithGoogle(account);
            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.w(TAG, "Google sign in failed", e);
                // ...
            }
        }
    }

    private void signIn(int methodLogin) {
        showProgress(true);

        switch (methodLogin) {
            case SharedPrefManager.GOOGLE_METHOD:
                Intent signInIntent = mGoogleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent, RC_SIGN_IN);
                break;
            case SharedPrefManager.FACEBOOK_METHOD:
                break;
            case SharedPrefManager.PHONE_METHOD:
                break;
            case SharedPrefManager.GUEST_METHOD:
                sharedPrefManager.saveSharedPrefString(SharedPrefManager.NAME_KEY, "Guest");
                sharedPrefManager.saveSharedPrefString(SharedPrefManager.EMAIL_KEY, "-");
                sharedPrefManager.saveSharedPrefString(SharedPrefManager.PHOTO_URL_KEY, "R.drawable.profiile_pic");
                sharedPrefManager.saveSharedPrefInt(SharedPrefManager.METHOD_KEY, SharedPrefManager.GUEST_METHOD);
                sharedPrefManager.saveSharedPrefBoolean(SharedPrefManager.IS_SIGNEDIN_KEY, true);
                startActivity(new Intent(this, MainActivity.class));
                finish();
                break;
        }
    }

    private void updateUI(FirebaseUser account) {
        if (FirebaseAuth.getInstance().getCurrentUser() != null) {
            startActivity(new Intent(SignInActivity.this, MainActivity.class));
            finish();
        }
    }

    private void showProgress(boolean show) {
        mSignInMethod.setVisibility(show?View.GONE:View.VISIBLE);
        mProgressBar.setVisibility(show?View.VISIBLE:View.INVISIBLE);
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (sharedPrefManager.getIsSignedIn()){
            startActivity(new Intent(SignInActivity.this, MainActivity.class)
                    .addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK));
            finish();
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}
