package com.xchannel.com.xchannel.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Video {
    @SerializedName("id")
    @Expose
    private String idVideo;
    @SerializedName("title")
    @Expose
    private String videoTitle;
    @SerializedName("thumbs")
    @Expose
    private String thumbVideo;
    @SerializedName("videoType")
    @Expose
    private String videoType;
    @SerializedName("videoURL")
    @Expose
    private String urlVideo;
    @SerializedName("description")
    @Expose
    private String deskripsiVideo;

    public String getIdVideo() {
        return idVideo;
    }

    public String getVideoTitle() {
        return videoTitle;
    }

    public String getThumbVideo() {
        return thumbVideo;
    }

    public String getVideoType() {
        return videoType;
    }

    public String getUrlVideo() {
        return urlVideo;
    }

    public String getDeskripsiVideo() {
        return deskripsiVideo;
    }
}
