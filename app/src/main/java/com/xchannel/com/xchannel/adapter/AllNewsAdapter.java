package com.xchannel.com.xchannel.adapter;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.xchannel.com.xchannel.ClickHandler;
import com.xchannel.com.xchannel.R;

import java.util.List;

public class AllNewsAdapter{
    /*private final List<Article> articleList;
    private final Context context;
    private final ClickHandler mClickHandler;

    public AllNewsAdapter(Context ctx, List<Article> data, ClickHandler clickHandler) {
        context = ctx;
        articleList = data;
        LayoutInflater mInflater = LayoutInflater.from(context);
        mClickHandler = clickHandler;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_news, parent, false);
        return new ItemViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        Glide.with(context)
                .load(articleList.get(position).getUrlToImage())
                .thumbnail(0.01f)
                .into(holder.image);
        holder.album.setText(articleList.get(position).getTitle());
    }

    @Override
    public int getItemCount() {
        //return articleList.size();
        return (articleList == null) ? 0 : articleList.size();

    }


    public class ItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView image;
        private TextView artist, album, count;
        private ImageButton btnMore;

        public ItemViewHolder(View itemView) {
            super(itemView);

            itemView.setOnClickListener(this);

            image = (ImageView) itemView.findViewById(R.id.imageMain);
            artist = (TextView) itemView.findViewById(R.id.artist);
            album = (TextView) itemView.findViewById(R.id.album);
            count = (TextView) itemView.findViewById(R.id.count);
        }

        @Override
        public void onClick(View v) {
            if (mClickHandler != null) mClickHandler.onItemClicked(getAdapterPosition());
        }
    }*/
}
