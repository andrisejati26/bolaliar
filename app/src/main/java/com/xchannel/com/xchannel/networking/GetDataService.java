package com.xchannel.com.xchannel.networking;

import com.xchannel.com.xchannel.model.HeadNews;
import com.xchannel.com.xchannel.model.Host;
import com.xchannel.com.xchannel.model.NewsEropa;
import com.xchannel.com.xchannel.model.NewsIndonesia;
import com.xchannel.com.xchannel.model.NewsInggris;
import com.xchannel.com.xchannel.model.NewsItalia;
import com.xchannel.com.xchannel.model.NewsJerman;
import com.xchannel.com.xchannel.model.NewsSpanyol;
import com.xchannel.com.xchannel.model.Video;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface GetDataService {



    @GET("newApi.php?limit=10")
    Call <List<HeadNews>> getAllHeadNews();


    @GET("newApi.php?limit=10&cat_id=7")
    Call <List<NewsEropa>> getAllNewsEropa();

    @GET("newApi.php?limit=10&cat_id=2")
    Call <List<NewsInggris>> getAllNewsInggris();

    @GET("newApi.php?limit=10&cat_id=1")
    Call <List<NewsIndonesia>> getAllNewsIndonesia();

    @GET("newApi.php?limit=10&cat_id=3")
    Call <List<NewsItalia>> getAllNewsItalia();

    @GET("newApi.php?limit=10&cat_id=4")
    Call <List<NewsSpanyol>> getAllNewsSpanyol();

    @GET("newApi.php?limit=10&cat_id=6")
    Call <List<NewsJerman>> getAllNewsJerman();




    @GET("newApi.php?limit=false&video=1")
    Call<List<Video>> getAllVideo();

    @GET("api/api.php?host_xchannel")
    Call<List<Host>> getAllHost();

}
