package com.xchannel.com.xchannel.ui.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import androidx.appcompat.widget.Toolbar;

import android.text.Html;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.Picasso;
import com.xchannel.com.xchannel.R;

import org.w3c.dom.Text;

import static com.xchannel.com.xchannel.R.id.html;
import static com.xchannel.com.xchannel.R.id.isiGambar;

public class ViewNewsActivity extends AppCompatActivity {

    private String JudulBerita;
    private String Isiberita;
    private String gambarberita;
    private Object Context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_prediksi);

        Toolbar toolbar = findViewById(R.id.toolbar);

        TextView txtjudul = findViewById(R.id.TextDeskripsi);
        TextView txtisi = findViewById(R.id.isiberita);

        ImageView isiGbr = findViewById(isiGambar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationIcon(R.drawable.ic_close_black_24dp);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                overridePendingTransition(R.anim.slide_in_up, R.anim.slide_out_up);
            }
        });

        Intent intent = getIntent();
        JudulBerita= intent.getStringExtra("judul");
        Isiberita = intent.getStringExtra("isiPost");
        gambarberita = intent.getStringExtra("gambar");

        Picasso.with(this).load(gambarberita).into(isiGbr);



        txtjudul.setText(JudulBerita);
        txtisi.setText(Html.fromHtml(Isiberita));





    }
}
