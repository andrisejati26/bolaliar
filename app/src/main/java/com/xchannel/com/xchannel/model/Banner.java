package com.xchannel.com.xchannel.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Banner {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("image_banner")
    @Expose
    private String imageBanner;
    @SerializedName("url_banner")
    @Expose
    private String urlBanner;

    public String getId() {
        return id;
    }

    public String getImageBanner() {
        return imageBanner;
    }

    public String getUrlBanner() {
        return urlBanner;
    }

}